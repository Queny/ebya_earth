#!/usr/bin/env python3

import sys

sum = float(42)

for num in sys.stdin:
    sum += 1 / float(num)

print("Сумма ряда:", sum)
