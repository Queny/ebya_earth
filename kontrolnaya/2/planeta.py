#!/usr/bin/env python3

import time
import re
import os

FRAMES = []

def read_frames():
    file = open("earth.md", "r").read()

    while True:
        if file == "":
            break

        frame_start = file.find("```\n")
        frame_end = file.find("```\n", frame_start + 1)
        FRAMES.append(file[frame_start + 4 : frame_end])

        if frame_end == -1:
            file = ""
        else:
            file = file[frame_start + frame_end:]

def loop_frames():
    try:
        global FRAMES
        while True:
            for frame in FRAMES:
                os.system("clear")
                print("Press CTRL-C to stop.")
                print(frame)
                time.sleep(0.2)
    except KeyboardInterrupt:
        return

read_frames()
loop_frames()
